ARG from=php:apache
FROM ${from}
ARG DEBIAN_FRONTEND=noninteractive

RUN apt update

ADD https://raw.githubusercontent.com/mlocati/docker-php-extension-installer/master/install-php-extensions /usr/local/bin/

RUN chmod uga+x /usr/local/bin/install-php-extensions && sync

RUN apt-get update -q \
    && apt-get install -qq -y \
      curl \
      git \
      zip unzip nodejs npm python-pip python-pymysql default-mysql-client libmariadbclient-dev \
    && install-php-extensions \
      bcmath \
      bz2 \
      calendar \
      exif \
      gd \
      intl \
      ldap \
      memcached \
      mysqli \
      opcache \
      pdo_mysql \
      pdo_pgsql \
      pgsql \
      redis \
      soap \
      xsl \
      zip \
      sockets \
    && a2enmod rewrite

RUN pip install mysqlclient pyhsm sqlalchemy

RUN mkdir app

WORKDIR /app

RUN npm update

RUN npm install -g npm@7.4.0

RUN npm install admin-lte --save